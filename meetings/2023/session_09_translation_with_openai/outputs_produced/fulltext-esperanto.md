# La Pythona Lernolibro

La Pythona estas facila lernebla potenca programlingvo. Ĝi havas efikajn datenstrukturojn de alta nivelo kaj simplan tamen efikan aliron al objektigitaj programado. La eleganta sintakso kaj dinamika ektipado, kune kun ĝia interpretita naturo, faras ĝin ideala lingvo por skriptado kaj rapida aplika disvolvado en multaj sekcioj sur la plejmultaj platformoj.

La Pythona interpretisto kaj la ampleksega norma biblioteko estas libere disponeblaj en fonta aŭ binara formo por ĉiuj ĉefaj platformoj el la Pythona retejo, [https://www.python.org/](https://www.python.org/), kaj povas esti libere distribuitaj. Same la retejo enhavas distribuaĵojn de kaj indikilojn al multaj liberaj triaj partaj Pythonaj moduloj, programoj kaj iloj, kaj plian dokumentadon.

La Pythona interpretisto facile estas etendita per novaj funkcioj kaj datenaj tipoj, realizaĵitaj en C aŭ C++ (aŭ aliaj lingvoj vokataj el C). La Pythona ankaŭ taŭgas kiel etendolingvo por personaĵe reguleblaj aplikaĵoj.

Ĉi tiu lernolibro tutmonde prezentas la bazon konceptojn kaj ecojn de la Pythona lingvo kaj sistemo. Ĝi faciligas, havi Pythonan interpretiston prete por viva sperto, tamen ĉiuj ekzemploj estas sendependa, do la lernolibro povas esti legata deŝtone kiel ankaŭ senrete.

Por priskribo de normaj objektoj kaj moduloj vidu [La Norma Biblioteko de Pythona](https://docs.python.org/3/library/index.html#library-index). [La Pythona Lingva Referenco](https://docs.python.org/3/reference/index.html#reference-index) donas pli formalan difinon de la lingvo. Por skribi etendojn en C aŭ C++, legu [Etendo kaj Altrokigadro de la Pythona Interpretisto](https://docs.python.org/3/extending/index.html#extending-index) kaj [Python/C API Referencolibro](https://docs.python.org/3/c-api/index.html#c-api-index). Same ekzistas pluraj libroj kovrantaj la Pythonan lingvon en profundecon.

Ĉi tiu lernolibro ne provas esti vasta kaj kovri ĉiujn trajtojn, eĉ ne ĉiujn kutime uzatajn trajtojn. Anstataŭe, ĝi prezentas multajn de la plej rimarkindaj trajtoj de la Pythona lingvo, kaj donas bonan ideon pri la gusto kaj stilo de la lingvo. Post legado, vi kapablos legadi kaj skribi Pythonajn modulojn kaj programojn, kaj vi pretos por lerni pli pri la pluraj Pythonaj bibliotekaj moduloj priskribitaj en [La Norma Biblioteko de Pythona](https://docs.python.org/3/library/index.html#library-index).