"""
    Pytest: generate parametrized tests.
"""
import ctypes
import sys

import pytest


def make_range(start: int, stop: int) -> list[int]:
    """
    Generate a range of intergers between the `start` and `stop`.
    """
    if not all((isinstance(start, int), isinstance(stop, int))):
        raise ValueError('Both `start` and `stop` must be integers.')
    if start > stop:
        raise ValueError('`start` cannot be greater than `stop`.')
    return list(range(start, stop + 1))


# ---

@pytest.mark.parametrize('start,stop', (
    ('10', '10'),
    (1, '10'),
    ('1', 10),
    (10, 1),
))
def test_range_input_validation(start, stop):
    """
    Test the input validation.
    """
    with pytest.raises(ValueError):
        make_range(start, stop)

@pytest.mark.parametrize('start,stop,legnth', (
    (1, 10, 10),
    (1, 1, 1),
))
def test_make_range(start, stop, legnth):
    """
    Test the range boundaries.
    """
    res = make_range(start, stop)
    assert isinstance(res, list)
    assert len(res) == legnth, 'The returned list length is incorrect.'
    assert res[0] == start
    assert res[-1] == stop

@pytest.mark.skip('This test is broken.')
def test_broken_functionality():
    """
    This test is disabled.
    """
    assert False

@pytest.mark.skipif(
    sys.platform == 'win32' and not ctypes.windll.shell32.IsUserAnAdmin(),
    reason='Administrator shell is required to test symlinks on Windows'
)
def test_delete_sys32_folder():
    """
    This test requires elevated admin rights in Windows.
    """
    assert True
