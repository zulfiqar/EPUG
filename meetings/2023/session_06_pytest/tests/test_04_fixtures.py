"""
    Pytest: fixtures.
"""
from email.message import EmailMessage
import smtplib
from unittest.mock import Mock

import pytest

def send_email(client: smtplib.SMTP, message: EmailMessage) -> None:
    """
    Send e-mail to the user.
    """
    client.send_message(message)

# --- conftest.py ---

@pytest.fixture(scope='function')
def email_subject():
    """
    Test e-mail subject.
    """
    return 'BEHOLD!'

@pytest.fixture(scope='function')
def email_message(email_subject):
    """
    A pre-formatted email message.
    """
    msg = EmailMessage()
    msg['Subject'] = email_subject
    msg['From'] = 'noreply@foobar.com'
    msg['To'] = 'support@embl.de'
    msg.set_content('Lay thine eyes upon the fileds.')
    return msg

@pytest.fixture(scope='function')
def smtp_client():
    """
    Return a fake SMTP client.
    """
    return Mock()

# ---

def test_send_email(smtp_client, email_message, email_subject):
    """
    Test the e-mail sender, the improved version.
    Note the arguments!
    """
    send_email(smtp_client, email_message)
    
    email_sent = smtp_client.mock_calls[0].args[0]
    assert email_sent['Subject'] == email_subject
