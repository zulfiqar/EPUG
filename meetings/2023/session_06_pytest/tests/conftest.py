"""
    Reusable test fixtures.
"""
import redis as _redis
import fakeredis
import pytest

@pytest.fixture(scope='function')
def test_redis():
    """
    Return a fake redis client.
    """
    # initialize fake storage
    redis = fakeredis.FakeStrictRedis(version=6)

    # alternative: use real Redis:
    # docker run --name redis -d -p 6379:6379 -e ALLOW_EMPTY_PASSWORD=yes bitnami/redis:latest
    # redis = _redis.from_url('redis://localhost:6379')
    # redis.flushall()
    yield redis


@pytest.fixture(scope='function')
def static_data(test_redis):
    """
    A static key always present in the cache.
    """
    # set the static key
    key, value = 'static', 'foo'
    test_redis.set(key, value)

    yield key, value.encode()

    # clean up
    test_redis.delete(key)
