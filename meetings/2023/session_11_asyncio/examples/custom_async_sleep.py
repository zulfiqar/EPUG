#!/usr/bin/env python

# asyncio.sleep() and time.sleep() are not equivalent! We can't just wrap
# time.sleep() in an async function, it will not be the same. `asyncio.sleep()` probably hijacks the event loop. Or maybe time.sleep() is blocking.
# custom_sleep() doesn't have any await, so it behaves like a synchronous function.

import asyncio
import time

async def custom_sleep(time_seconds):
    time.sleep(time_seconds)
    return

async def test_custom_sleep():
    await custom_sleep(1)
    return 1

async def test_asyncio_sleep():
    await asyncio.sleep(1)
    return 1

async def main():
    print("testing custom sleep function")
    start = time.perf_counter()
    await asyncio.gather(test_custom_sleep(), test_custom_sleep())
    print("Duration: ", time.perf_counter() - start)

    print("testing asyncio sleep function")
    start = time.perf_counter()
    await asyncio.gather(test_asyncio_sleep(), test_asyncio_sleep())
    print("Duration: ", time.perf_counter() - start)

    # print("On the other hand...")
    # start = time.perf_counter()
    # await asyncio.gather(custom_sleep(1), custom_sleep(1))
    # print("Duration: ", time.perf_counter() - start)


asyncio.run(main())
