# Profilers

- Austin (CPU, Memory and Wall time profiler) - https://github.com/P403n1x87/austin
- Austin VS Code extension - https://marketplace.visualstudio.com/items?itemName=p403n1x87.austin-vscode

## VS Code task configuration

Store in `.vscode/tasks.json`:

```json
{
    "version": "2.0.0",
    "tasks": [
        {
            "type": "austin",
            "file": "profile-start.py",
            "args": [
                "--efficient"
            ],
            "label": "Efficient sort profiling",
            "problemMatcher": [],
            "group": "build"
        },
        {
            "type": "austin",
            "file": "profile-start.py",
            "args": [
                "--slow"
            ],
            "label": "Slow sort profiling",
            "problemMatcher": [],
            "group": "build"
        },
        {
            "type": "austin",
            "file": "profile-start.py",
            "args": [
                "--idle"
            ],
            "label": "Idle no-sort profiling",
            "problemMatcher": [],
            "group": "build"
        }
    ]
}

```

## Other projects

- memray (memory profiler) - https://github.com/bloomberg/memray
