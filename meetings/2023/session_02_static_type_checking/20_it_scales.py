"""
█ ▀█▀  █▀ █▀▀ ▄▀█ █░░ █▀▀ █▀
█ ░█░  ▄█ █▄▄ █▀█ █▄▄ ██▄ ▄█
"""

# "- SCALES OVER ENTIRE PROJECT"
# "    - you can't keep everything in your head"
# "    - once a function is typed-checked, you can forget about"
# "      its implementation"
# "- SCALES ACCROSS LIBRARIES"
# "- SCALES ACROSS TIME"
# "- SCALES ACROSS PEOPLE"