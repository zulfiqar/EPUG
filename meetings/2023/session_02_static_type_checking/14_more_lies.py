"""
█▀▄▀█ █▀█ █▀█ █▀▀   █░░ █ █▀▀ █▀
█░▀░█ █▄█ █▀▄ ██▄   █▄▄ █ ██▄ ▄█
"""
def make_http_request(url: str):
    ...

make_http_request("https://news.ycombinator.com/")

# """
#  █▀▀ ▄▀█ █▄░█   █   █▀▄ █▀█   ▀█▀ █░█ █ █▀ ▀█
#  █▄▄ █▀█ █░▀█   █   █▄▀ █▄█   ░█░ █▀█ █ ▄█ ░▄
# """
# make_http_request("wild raccoons")