"""
█▄▄ █▀▀ ▀█▀ ▀█▀ █▀▀ █▀█  ▀█▀ █░█ ▄▀█ █▄░█  ▀█▀ █▀▀ █▀ ▀█▀ █▀
█▄█ ██▄ ░█░ ░█░ ██▄ █▀▄  ░█░ █▀█ █▀█ █░▀█  ░█░ ██▄ ▄█ ░█░ ▄█


'Testing can show the presence of bugs, but not their absence'
--------------------------------------------------------------
"""

"- If you test for good values, you find *some* inputs"
"  that work with your code"

"     - If you type-check, you guarantee that all valid inputs"
"       will work with your code"


" - If you test for bad values, it means you found *some* inputs"
"   that don't work for your code."

"     - If you type-check, most (if not all) of those failures"
"       would not even be allowed to exist"
