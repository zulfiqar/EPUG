"""
█▀ █▀▀ ▄▀█ ▀█▀ █▄▄ █▀▀ █░░ ▀█▀ █▀
▄█ ██▄ █▀█ ░█░ █▄█ ██▄ █▄▄ ░█░ ▄█
"""

"- in the end, type-safety it's like a seatbelts:"

"    - it won't save you from an incoming truck"
"      carrying 1000000 bricks..."

"    - but you should still use them"