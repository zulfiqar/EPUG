"""
█▀ █░█ ▄▀█ █▀█ █▀▀   █▄█ █▀█ █░█ █▀█   █▀▀ █▀█ █▀▄ █▀▀
▄█ █▀█ █▀█ █▀▄ ██▄   ░█░ █▄█ █▄█ █▀▄   █▄▄ █▄█ █▄▀ ██▄
"""

"- If your code is type-safe, it's way easier to share"

"- Because it states its intent and interfaces clearly"

"- And if you change anything, it's easier to detect"
"  that you'll be causing breakages:"

class Dataset:
    ...
class Predictions:
    ...

def classifiy_pixels(dataset: Dataset) -> Predictions:
    ...

# def classify_pixels(dataset: Dataset, halo: int) -> Predictions:
#     ...

predictions = classifiy_pixels(Dataset())