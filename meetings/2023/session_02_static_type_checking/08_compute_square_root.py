"JUST STATE WHAT YOU NEED AND WHAT YOU PROVIDE:"

def compute_square_root(value: float) -> float:
    import math
    return math.sqrt(value)


"IS THIS THAT MUCH BETTER THAN THIS, THOUGH?"

def compute_square_root2(value):
    import math
    return math.sqrt(value)