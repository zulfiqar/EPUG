"""
█▄▀ █▀▀ █▀▀ █▀█ █▀  █▄█ █▀█ █░█  █░█ █▀█ █▄░█ █▀▀ █▀ ▀█▀
█░█ ██▄ ██▄ █▀▀ ▄█  ░█░ █▄█ █▄█  █▀█ █▄█ █░▀█ ██▄ ▄█ ░█░

- IF YOUR FUNCTIONS SAY YOU'RE GONNA PRODUCE
  A "Frobnificator", THEN YOU ACTUALLY HAVE TO.
"""