from typing import Sequence, Union, Mapping

"""
Composing doesn't work well if from_json expects the raw json string.
Let's try something more flexible:
"""

JsonLeafValue = Union[int, float, str, bool, None]
# e.g.: 1, 3.14, "hello", True, None

JsonObject = Mapping[str, "JsonValue"]
#e.g.: {"a": 1, "b": 3.14, "c": "hello", "d": True, "e": None}

JsonArray = Sequence["JsonValue"]
#e.g: [
#    1,
#    2.3,
#    "hello",
#    True,
#    None,
#    {"bla": 123},
#    ["another", "array", 123]
#]

JsonValue = Union[JsonLeafValue, JsonArray, JsonObject]
# e.g.: any of the above