"INTRO"

"""
    - This talk is based on static type checking (i.e. pyright, mypy);
        - If you're not using static type hints, give it a shot =D

    - But static types only really exist INSIDE your program;

    - Anything going IN or OUT of your program can't be trusted ;

    - It is hard to do serialization right;

    - And it is harder to write in a way that can be meaningfully
      checked by pyright/mypy
"""