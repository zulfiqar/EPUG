# pyright: strict
from dataclasses import dataclass
from collections.abc import Mapping
from slide_06_json_types import JsonObject, JsonValue

@dataclass
class Person:
    name: str
    age: int # NEW FIELD!

    def to_json(self) -> JsonObject:
        return {
            "name": self.name,
            # OOPS! Forgot to add age here!
        }

    @classmethod
    def from_json(cls, data: JsonValue) -> "Person":
        if not isinstance(data, Mapping):
            raise ValueError("Expected mapping")
        name = data["name"]
        if not isinstance(name, str):
            raise ValueError("Expected string")
        age = data["age"] # THIS WILL ALWAYS FAIL!
        if not isinstance(age, int):
            raise ValueError("Expected int")
        return Person(name=name, age=age)

# Insight: Serialization can go out of sync with deserialization
# because serialization is not "type-unsafe". It can output
# whatever it wants