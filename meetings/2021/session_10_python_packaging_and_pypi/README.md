# Python Packaging and PyPI

Do you have some Python code around that you'd would like to get ready for publication, share with an external collaborator or the world? Today's session is for you!
Martin Larralde will guide us through the hoops of packaging Python code and show us how to make it available in the central [Python Package Index (PyPI)](https://pypi.org/).
PyPI is the platform that allows us to `pip install` all those super useful Python libraries. 

The example project is available internally in the [larralde/epug-package](https://git.embl.de/larralde/epug-package) repository.

## Some useful links

* `setuptools` documentation: https://setuptools.readthedocs.io/en/latest/
* `setup.cfg` reference: https://setuptools.readthedocs.io/en/latest/userguide/declarative_config.html
* My personal `setup.cfg` template with a lot of pre-configured things: https://gist.github.com/althonos/6914b896789d3f2078d1e6237642c35c
* `pip` documentation: https://pip.pypa.io/
* A friendly website to choose a license: https://choosealicense.com/