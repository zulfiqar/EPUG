# Image processing server

In this session Marco Cosenza presented the FastAPI RESTful server he has been working on since the first lockdown.

The code is available internally at: https://git.embl.de/cosenza/image_processing_api

## Other topics discussed

- https://fastapi.tiangolo.com/
- https://swagger.io/
- https://python-rq.org/
