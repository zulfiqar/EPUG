# EMBL Python User Group

The EMBL Python User Group (EPUG) is a group of people at EMBL Heidelberg who share an interest in the Python programming language. 
You can contact the group by sending email to [python@embl.de](mailto:python@embl.de). 
That mailing list is managed by [Renato Alves](mailto:renato.alves@embl.de) -
to join EPUG, please send him an email. 
Meetings usually take place fortnightly, on Tuesdays at 16:00:
see the full schedule [here](https://docs.google.com/spreadsheets/d/1cMDumZ8GdhsTaiWzJl1qHDfi192sl4P1KJJhtuk0ZkE/edit?usp=sharing).

## Adding material to this repository

Material for each session should be added in a new folder,
with a name describing the main topic covered in that session.
Material from multiple sessions on a similar topic can be combined
together in a single folder.

Material can be added to this repository by merge request. 
To gain push access to the repository,
and for guidance on creating your merge request,
please contact one of the EPUG organisers:

- Wasiu Akanni
- Renato Alves

## License

Material under this repository is licensed under the MIT license unless another license is explicitly mentioned.